package ra.bt3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class DataProject {
    private Student student = new Student();
    private Student student1 = new Student("Lớp 1");
    private Student student2 = new Student("Quang", 24, "bê đê", "lớp 1", 8.5f);
    private Teacher teacher = new Teacher();
    private Teacher teacher1 = new Teacher(true);
    private Teacher teacher2 = new Teacher(30, false);
}
