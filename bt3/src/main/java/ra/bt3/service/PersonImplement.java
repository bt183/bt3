package ra.bt3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ra.bt3.*;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonImplement implements PersonInterface{

    @Autowired
    DataProject dataProject;
    @Override
    public ResponseApi getStudent() {
        List<Student> content = new ArrayList<>();
        content.add(dataProject.getStudent());
        content.add(dataProject.getStudent1());
        content.add(dataProject.getStudent2());
        return new ResponseApi(content, "Đây là 3 constructor Student", true);
    }

    @Override
    public ResponseApi getTeacher() {
        List<Teacher> content = new ArrayList<>();
        content.add(dataProject.getTeacher());
        content.add(dataProject.getTeacher1());
        content.add(dataProject.getTeacher2());
        return new ResponseApi(content, "Đây là 3 constructor Teacher", true);
    }

    @Override
    public ResponseApi getStudentAndTeacher() {
        List content = new ArrayList<>();
        content.add(dataProject.getStudent());
        content.add(dataProject.getStudent1());
        content.add(dataProject.getStudent2());
        content.add(dataProject.getTeacher());
        content.add(dataProject.getTeacher1());
        content.add(dataProject.getTeacher2());
        return new ResponseApi(content, "Đây tất cả constructor của giáo viên và học sinh", true);
    }

    @Override
    public ResponseApi getPersonHello() {
        return null;
    }

    @Override
    public ResponseApi getStudentHello() {
        List<String> content = new ArrayList<>();
        content.add(dataProject.getStudent().printHello());
        content.add(dataProject.getStudent1().printHello(18,"ĐM Quang"));
        return new ResponseApi(content, "2 phương thức 1 Override và 1 Overload ", true);
    }

    @Override
    public ResponseApi getTeacherHello() {
        List<String> content = new ArrayList<>();
        content.add(dataProject.getTeacher().printHello());
        content.add(dataProject.getTeacher().printHello(30, true));
        return new ResponseApi(content, "2 phương thức 1 Override và 1 Overload ", true);
    }
}
