package ra.bt3.service;

import org.springframework.stereotype.Service;
import ra.bt3.ResponseApi;

@Service
public interface PersonInterface {
    public ResponseApi getStudent ();

    public ResponseApi getTeacher();

    public ResponseApi getStudentAndTeacher();

    public ResponseApi getPersonHello();

    public ResponseApi getStudentHello();

    public ResponseApi getTeacherHello();
}
