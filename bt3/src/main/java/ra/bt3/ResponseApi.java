package ra.bt3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor

public class ResponseApi {
    private List content;
    private String message;
    private boolean status;
}
