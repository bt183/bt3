package ra.bt3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

public abstract class Person {
    private String name;
    private int age;
    private String gender;


    // 3 contructor
    public Person(){}

    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, String gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String printHello (){
       return "Hello ,i am a Person!";
    }

    public void hello() {
        System.out.println("Person here!");
    }

}
