package ra.bt3;

import lombok.Data;

@Data
public class Teacher extends Person {
    private int career;
    private boolean marital;

    public Teacher(){};

    public Teacher(boolean marital){
        this.marital = marital;
    }

    public Teacher( int career, boolean marital){
        super(); // dùng super() không truyền vào là gọi đến contructor k tham số của class kế thừa gần nhất (person)
                // nếu k gọi thì java tự động gọi cho mình để có thể khởi tạo các thuộc tính kế thừa từ tk cha.
                // truyền tham số thì gọi đến contructor có tham số của tk cha.
        this.marital = marital;
        this.career = career;
    }

    @Override
    public String printHello (){
       return "Hello ,i am a Teacher!";
    }

    public String printHello (int career, boolean marital) {
        return "Hello, i am a teacher, I have been doing this job for "
                + career + " years, " + " I am married? " + marital;
    }

    @Override
    public void hello() {
        System.out.println("Co giao tot!");
    }

    public void hello(int career, boolean marital) {
        System.out.println("Cô giáo " + marital + " có chồng, và có tuổi nghề " + career + " năm!");
    }
}
