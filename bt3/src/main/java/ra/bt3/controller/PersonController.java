package ra.bt3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ra.bt3.ResponseApi;
import ra.bt3.service.PersonInterface;

@CrossOrigin
@RestController
@RequestMapping ("/person")
public class PersonController {
    @Autowired
    PersonInterface personInterface;
    @GetMapping ("/student")
    public ResponseEntity <ResponseApi> getStudent () {
        return new ResponseEntity<>(personInterface.getStudent(), HttpStatus.OK);
    }

    @GetMapping ("/teacher")
    public ResponseEntity <ResponseApi> getTeacher() {
        return new ResponseEntity<>(personInterface.getTeacher(), HttpStatus.OK);
    }

    @GetMapping ("/all")
    public ResponseEntity <ResponseApi> getAllData() {
        return new ResponseEntity<>(personInterface.getStudentAndTeacher(),HttpStatus.OK);
    }

    @GetMapping ("/helloStudent")
    public ResponseEntity <ResponseApi> getHelloStudent() {
        return new ResponseEntity<>(personInterface.getStudentHello(), HttpStatus.OK);
    }

    @GetMapping ("/helloTeacher")
    public ResponseEntity <ResponseApi> getHelloTeacher() {
        return new ResponseEntity<>(personInterface.getTeacherHello(), HttpStatus.OK);
    }
}
