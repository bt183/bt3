package ra.bt3;

import lombok.Data;

@Data

public class Student extends Person {
    private String classroom;
    private float point;

    public Student () {}

    public Student (String classroom){
        this.classroom = classroom;
    }

    public Student (String name, int age, String gander,String classroom, float point) {
        super(name,age,gander); // goi den contructor tk cha bang super() va truyen vao
        // cac gia tri trong ham khoi tao tk cha can
        // neu khong truyen thi no se goi contructor khong tham so cua tk cha
        this.classroom = classroom;
        this.point = point;
    }

    @Override
    public String printHello (){
         return "Hello ,i am a Student!";
    }

    public String printHello (int age, String name) {
        return "Hello, I am a student, my name is " + name + ", i am " + age + " years old!";
    }

    @Override
    public void hello() {
        System.out.println("Hoc sinh ngoan!");
    }

    public void hello(String classroom, float point) {
        System.out.println("Học sinh lớp " + classroom + " được điểm " + point);
    }
}
